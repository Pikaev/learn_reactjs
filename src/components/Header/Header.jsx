import React from 'react'
import s from './Header.module.css'

const Header = () => {
    return <header className={s.header}>
        <img src='https://catherineasquithgallery.com/uploads/posts/2021-02/1612895709_6-p-zvezda-na-krasnom-fone-8.png' alt="logo" />
    </header>
}

export default Header