import React from 'react'
import Posts from './Posts/Posts'
import s from './Profile.module.css'

const Profile = () => {
    return (
        <div className={s.content}>
            <div>
                <img src='https://fuzeservers.ru/wp-content/uploads/8/1/3/813841571831c8e1781a523cfcf3d720.png'
                     alt='alt'/>
            </div>
            <div>
                ava + description description description
            </div>
            <Posts/>
        </div>
    )
}

export default Profile
