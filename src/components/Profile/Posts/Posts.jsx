import React from 'react'
import Post from './Post/Post'
import s from './Posts.module.css'

const Posts = () => {
    function getRandom() {
        return Math.floor(Math.random() * 10)
    }
    return (
        <div>
            My posts
            <div className={s.title}>
                New post
            </div>
            <div>
                <Post like={getRandom()} dislike={getRandom()} />
                <Post like={getRandom()} dislike={getRandom()} />
                <Post like={getRandom()} dislike={getRandom()} />
                <Post like={getRandom()} dislike={getRandom()} />
                <Post like={getRandom()} dislike={getRandom()} />
            </div>
        </div>
    )
}

export default Posts 