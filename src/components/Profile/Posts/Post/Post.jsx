import React from 'react'
import s from './Post.module.css'

const Post = (props) => {
    return (
        <div>
            <p className={s.title}>Первый пост!!!</p>
            <span>{props.like} like</span>&nbsp;<span>{props.dislike} dislike</span>
        </div>
    )
}

export default Post