import React from 'react'
import {Routes, Route, NavLink } from 'react-router-dom'

import s from './Navbar.module.css'

import Profile from "../Profile/Profile"
import Messages from "../Messages/Messages"
import News from "../News/News"
import Music from "../Music/Music"
import Setting from "../Setting/Setting"
import Notfoundpage from "../Notfoundpage/Notfoundpage"

const setActive = ({isActive}) => isActive ? s.active : ''

const Navbar = () => {
    return (
        <>
            <nav className={s.nav}>
                <container className={s.wrapper}>
                    <div className={s.item}>
                        <NavLink to='/profile' className={setActive}>Profile</NavLink>
                    </div>
                    <div className={s.item}>
                        <NavLink to='/messages' className={setActive}>Messages</NavLink>
                    </div>
                    <div className={s.item}>
                        <NavLink to='/news' className={setActive}>News</NavLink>
                    </div>
                    <div className={s.item}>
                        <NavLink to='/music' className={setActive}>Music</NavLink>
                    </div>
                    <div className={s.item}>
                        <NavLink to='/settings' className={setActive}>Settings</NavLink>
                    </div>
                </container>
            </nav>
            <Routes>
                <Route path='/' element={<Profile/>}/>
                <Route path='/profile' element={<Profile/>}/>
                <Route path='/messages' element={<Messages/>}/>
                <Route path='/news' element={<News/>}/>
                <Route path='/music' element={<Music/>}/>
                <Route path='/settings' element={<Setting/>}/>
                <Route path='*' element={<Notfoundpage/>}/>
            </Routes>
        </>
    )
}

export default Navbar
