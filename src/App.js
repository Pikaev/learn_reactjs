import React from 'react'
import './App.css'

import Header from './components/Header/Header'
import Navbar from './components/Navbar/Navbar'
import Footer from "./components/Footer/Footer"

const App = () => {
  return (
    <div className='app-wrapper'>
      <Header />
      <Navbar />
      <Footer />
    </div>)
}

export default App
